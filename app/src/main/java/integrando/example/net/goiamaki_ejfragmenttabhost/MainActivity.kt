package integrando.example.net.goiamaki_ejfragmenttabhost

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTabHost
import android.widget.TabHost
import android.widget.Toast

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        try {
            var tabHost: FragmentTabHost = findViewById(android.R.id.tabhost)
            tabHost.setup(this, supportFragmentManager, android.R.id.tabcontent)

            var res: Resources = resources
            var tab1: TabHost.TabSpec = tabHost.newTabSpec("tab1")
            var tab2: TabHost.TabSpec = tabHost.newTabSpec("tab2")
            var tab3: TabHost.TabSpec = tabHost.newTabSpec("tab3")

            tab1.setIndicator(res.getString(R.string.eti_tab1), null)
            tab2.setIndicator(res.getString(R.string.eti_tab2), null)
            tab3.setIndicator(res.getString(R.string.eti_tab3), null)

            tabHost.addTab(tab1, FragmentPestanya1::class.java, null)
            tabHost.addTab(tab2, FragmentPestanya2::class.java, null)
            tabHost.addTab(tab3, FragmentPestanya3::class.java, null)
        } catch (e:Exception){
            Toast.makeText(this,e.message,Toast.LENGTH_LONG).show()
        }
    }
}
