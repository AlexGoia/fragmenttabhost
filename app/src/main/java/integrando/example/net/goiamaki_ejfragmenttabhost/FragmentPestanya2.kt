package integrando.example.net.goiamaki_ejfragmenttabhost

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * Created by Alex on 07/10/2017.
 */
class FragmentPestanya2 : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v : View = inflater!!.inflate(R.layout.pestanya2_layout, container, false)
        var tv : TextView = v.findViewById(R.id.text)
        var res : Resources = resources
        tv.text = res.getString(R.string.eti_tab2)
        return v
    }
}